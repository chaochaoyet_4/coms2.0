/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//【客戶編號】驗證
jQuery.validator.addMethod ("isCustomerId", function(value,element){
var regexp = /^CUS20[0-9]{2}[0|1]{1}[0-9]{1}[0|1|2|3]{1}[0-9]{4}$/;
return (regexp.test(value));
},"編號格式為CUSYYYYMMDDxxx");

jQuery.validator.addMethod ("isCustomerId_", function(value,element){
var regexp = /^CUS20[0-9]{2}[0|1]{1}[0-9]{1}[0|1|2|3]{1}[0-9]{4}$/;
return this.optional(element) || (regexp.test(value));
},"編號格式為CUSYYYYMMDDxxx");

//【產品編號】驗證
jQuery.validator.addMethod ("isProductId", function(value,element){
var regexp =  /^PRO20[0-9]{2}[0|1]{1}[0-9]{1}[0|1|2|3]{1}[0-9]{4}$/;
return (regexp.test(value));
},"編號格式為PROYYYYMMDDxxx");

jQuery.validator.addMethod ("isProductId_", function(value,element){
var regexp =  /^PRO20[0-9]{2}[0|1]{1}[0-9]{1}[0|1|2|3]{1}[0-9]{4}$/;
return this.optional(element) || (regexp.test(value));
},"編號格式為PROYYYYMMDDxxx");

//【訂單表頭編號】驗證
jQuery.validator.addMethod ("isOrdHeadId", function(value,element){
var regexp =  /^ORDH20[0-9]{2}[0|1]{1}[0-9]{1}[0|1|2|3]{1}[0-9]{4}$/;
return (regexp.test(value));
},"編號格式為ORDHYYYYMMDDxxx");

jQuery.validator.addMethod ("isOrdHeadId_", function(value,element){
var regexp =  /^ORDH20[0-9]{2}[0|1]{1}[0-9]{1}[0|1|2|3]{1}[0-9]{4}$/;
return this.optional(element) || (regexp.test(value));
},"編號格式為ORDHYYYYMMDDxxx");



//【名稱】驗證：
jQuery.validator.addMethod ("isName", function(value,element){
var regexp = /^[A-Za-z0-9\u4e00-\u9fa5]+$/;
return (regexp.test(value));
},"只能是中文、英文或數字");

jQuery.validator.addMethod ("isName_", function(value,element){
var regexp = /^[A-Za-z0-9\u4e00-\u9fa5]+$/;
return this.optional(element) || (regexp.test(value));
},"只能是中文、英文或數字");

//【郵箱】驗證：
jQuery.validator.addMethod ("isMail", function(value,element){
var regexp =   /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
return (regexp.test(value));
}," 郵箱格式錯誤");

jQuery.validator.addMethod ("isMail_", function(value,element){
var regexp =   /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
return this.optional(element) || (regexp.test(value));
},"郵箱格式錯誤");

//【電話】驗證：
jQuery.validator.addMethod ("isPhone", function(value,element){
var regexp =   /^[1][358]\d{9}$/;
return (regexp.test(value));
},"電話號碼格式錯誤");

jQuery.validator.addMethod ("isPhone_", function(value,element){
var regexp =   /^[1][358]\d{9}$/;
return this.optional(element) || (regexp.test(value));
},"電話號碼格式錯誤");


//【規格】驗證：
jQuery.validator.addMethod ("isSpec", function(value,element){
var regexp = /^[A-Za-z0-9\u4e00-\u9fa5]+$/;
return (regexp.test(value));
},"只能是中文、英文或數字");

jQuery.validator.addMethod ("isSpec_", function(value,element){
var regexp = /^[A-Za-z0-9\u4e00-\u9fa5]+$/;
return this.optional(element) || (regexp.test(value));
},"只能是中文、英文或數字");

//【價格】驗證：
jQuery.validator.addMethod ("isPrice", function(value,element){
var regexp =  /^\d{1,5}(\.\d{1,3})?$/;
return (regexp.test(value));
},"只能是整數(5)或小數(3)");

jQuery.validator.addMethod ("isPrice_", function(value,element){
var regexp =  /^\d{1,5}(\.\d{1,3})?$/;
return this.optional(element) || (regexp.test(value));
},"只能是整數(5)或小數(3)");

//【日期】驗證
jQuery.validator.addMethod ("isDate", function(value,element){
var regexp = /^\d{4}[-/.]\d{2}[-/.]\d{2}$/;
return (regexp.test(value));
},"日期格式為YYYY/MM/DD");

jQuery.validator.addMethod ("isDate_", function(value,element){
var regexp = /^\d{4}[-/.]\d{2}[-/.]\d{2}$/;
return this.optional(element) || (regexp.test(value));
},"日期格式為YYYY/MM/DD");

//【數量】驗證
jQuery.validator.addMethod ("isQuantity", function(value,element){
var regexp = /^(0|[1-9]\d{0,10})$/;
return (regexp.test(value));
},"只能是整數(11)");

jQuery.validator.addMethod ("isQuantity_", function(value,element){
var regexp = /^(0|[1-9]\d{0,10})$/;
return this.optional(element) || (regexp.test(value));
},"只能是整數(11)");

//【區間】驗證：
jQuery.validator.addMethod ("isSection", function(value,element){
var regexp = /^(0|[1-9]\d{0,10})$/;
return (regexp.test(value));
},"只能是整數(11)");

jQuery.validator.addMethod ("isSection_", function(value,element){
var regexp = /^(0|[1-9]\d{0,10})$/;
return this.optional(element) || (regexp.test(value));
},"只能是整數(11)");