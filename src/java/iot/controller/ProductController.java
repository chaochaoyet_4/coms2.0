/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iot.controller;

import iot.dao.entity.Customer;
import iot.dao.entity.CustomerPrice;
import iot.dao.entity.Product;
import iot.dao.repository.CustomerDAO;
import iot.response.Response;
import iot.service.CustomerPriceService;
import iot.service.ProductService;
import java.util.HashMap;
import java.util.List;
import javax.persistence.EntityManagerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author dell
 */
@Controller
@RequestMapping("/Product")
public class ProductController {
    
    @Autowired 
    private ProductService productService;
    
    //創建實體管理工廠的對象
    @Autowired
    private EntityManagerFactory emf;
    
    //創建CustomerPriceService對象
    @Autowired
    private CustomerPriceService customerPriceService;
    
    /*******************************************************************************
     * 建立者：Guardp  建立日期：-  最後修訂日期：-
     * 功能簡述：跳轉到產品頁面，查詢第一頁的資訊
     * 
     * @param productIdMin
     * @param productIdMax
     * @param productName
     * @param productPriceMin
     * @param productPriceMax
     * @param productSpec
     * @param pageNo
     * @param modelMap
     * @return 
     ********************************************************************************/
    @RequestMapping("ProductQuery")
    public String productMasterQuery(@RequestParam(value = "productIdMin",defaultValue = "") String productIdMin,
        @RequestParam(value = "productIdMax",defaultValue = "") String productIdMax ,
        @RequestParam(value = "productName",defaultValue = "")String productName, 
        @RequestParam(value = "productPriceMin",defaultValue = "")String productPriceMin,
        @RequestParam(value = "productPriceMax",defaultValue = "")String productPriceMax,
        @RequestParam(value = "productSpec",defaultValue = "")String productSpec,
        @RequestParam(value = "pageNo",defaultValue = "1")String pageNo,ModelMap modelMap){
        //將查詢條件保存到map中
        HashMap<String,String> queryCondition = new HashMap<>();
        queryCondition.put("productIdMin", productIdMin);
        queryCondition.put("productIdMax", productIdMax);
        queryCondition.put("productName", productName);
        queryCondition.put("productPriceMin", productPriceMin);
        queryCondition.put("productPriceMax", productPriceMax);
        queryCondition.put("productSpec", productSpec);
        queryCondition.put("pageNo", pageNo);
        
        //調用Service中的ProductQuery查詢方法，並將結果返回到Response的對象中
        Response productQueryResponseList = productService.ProductQuery(productIdMin,productIdMax,productName,
                productPriceMin,productPriceMax,productSpec,pageNo);
        Response productCountResponseList = productService.ProductCountQuery(productIdMin,productIdMax,productName,
                productPriceMin,productPriceMax,productSpec);
        
        List<Product> productList = (List<Product>) productQueryResponseList.getData();
        List<Product> pList = (List<Product>) productCountResponseList.getData();
        if(productList.size() == 0){
            modelMap.addAttribute("message", "此范圍內沒有您所需查詢的產品！！！");
            return "ProductManage";
        }
        int totalPages = (pList.size()-1)/10 + 1;
        modelMap.addAttribute("productList", productList);
        modelMap.addAttribute("queryCondition", queryCondition);
        modelMap.addAttribute("totalPages", totalPages);
        
        return "ProductManage";        
    }
    
	/*******************************************************************************
     * 建立者：Guardp  建立日期：-  最後修訂日期：-
     * 功能簡述：跳轉到產品頁面，查詢第一頁的資訊
     * 
     * @param productIdMin
     * @param productIdMax
     * @param productName
     * @param productPriceMin
     * @param productPriceMax
     * @param productSpec
     * @param pageNo
     * @param modelMap
     * @return 
     ********************************************************************************/
    @RequestMapping("addProduct")
    @ResponseBody
    public HashMap<String, String> addProduct(@RequestParam("productStandardPrice") String productStandardPrice ,
        @RequestParam("productName")String productName,@RequestParam("discountStatus")boolean discountStatus,
        @RequestParam("productSpec")String productSpec){
        HashMap<String,String> customerPriceMap = productService.addProductMasterService(Float.parseFloat(productStandardPrice), productName, discountStatus, productSpec);
        return customerPriceMap;     
    }
    
	/*******************************************************************************
     * 建立者：Guardp  建立日期：-  最後修訂日期：-
     * 功能簡述：跳轉到產品頁面，查詢第一頁的資訊
     * 
     * @param productIdMin
     * @param productIdMax
     * @param productName
     * @param productPriceMin
     * @param productPriceMax
     * @param productSpec
     * @param pageNo
     * @param modelMap
     * @return 
     ********************************************************************************/
    @RequestMapping("queryCustomer")
    @ResponseBody
    public List<Customer> queryCustomer(){
        CustomerDAO customerDAO = new CustomerDAO(emf);
        Response customerQueryList = customerDAO.findCustomer();
        List<Customer> customerList = (List<Customer>) customerQueryList.getData();
        return customerList;
    }
    
	/*******************************************************************************
     * 建立者：Guardp  建立日期：-  最後修訂日期：-
     * 功能簡述：跳轉到產品頁面，查詢第一頁的資訊
     * 
     * @param productIdMin
     * @param productIdMax
     * @param productName
     * @param productPriceMin
     * @param productPriceMax
     * @param productSpec
     * @param pageNo
     * @param modelMap
     * @return 
     ********************************************************************************/
    @RequestMapping("setCustomerPrice")
    @ResponseBody
    public CustomerPrice setCustomerPrice(@RequestParam("productMasterId") Product productMasterId,@RequestParam("customerMasterId") Customer customerMasterId,
            @RequestParam("rangeMin") String rangeMin,@RequestParam("rangeMax") String rangeMax,@RequestParam("rangePrice") String rangePrice) throws Exception{
        return customerPriceService.setCustomerPrice(productMasterId,customerMasterId,Integer.parseInt(rangeMin), Integer.parseInt(rangeMax), Float.parseFloat(rangePrice));
    }
    
	/*******************************************************************************
     * 建立者：Guardp  建立日期：-  最後修訂日期：-
     * 功能簡述：跳轉到產品頁面，查詢第一頁的資訊
     * 
     * @param productIdMin
     * @param productIdMax
     * @param productName
     * @param productPriceMin
     * @param productPriceMax
     * @param productSpec
     * @param pageNo
     * @param modelMap
     * @return 
     ********************************************************************************/
    @RequestMapping("modifyProduct")
    @ResponseBody
    public HashMap<String, String> modifyProduct(@RequestParam("productId") String productId,@RequestParam("productStandardPrice") String productStandardPrice,
    @RequestParam("discountStatus") boolean discountStatus,String versionNumber) throws Exception{
        HashMap<String,String> customerPriceMap = productService.modifyProduct(productId,Float.parseFloat(productStandardPrice),discountStatus,Integer.parseInt(versionNumber));
        return customerPriceMap; 
    }
    
	/*******************************************************************************
     * 建立者：Guardp  建立日期：-  最後修訂日期：-
     * 功能簡述：跳轉到產品頁面，查詢第一頁的資訊
     * 
     * @param productIdMin
     * @param productIdMax
     * @param productName
     * @param productPriceMin
     * @param productPriceMax
     * @param productSpec
     * @param pageNo
     * @param modelMap
     * @return 
     ********************************************************************************/
    @RequestMapping("modifyCustomerPrice")
    @ResponseBody
    public List<CustomerPrice> modifyCustomerPrice(@RequestParam("productMasterId") Product productMasterId,@RequestParam("customerMasterId") Customer customerMasterId,
            @RequestParam("rangeMin") String rangeMin,@RequestParam("rangeMax") String rangeMax,@RequestParam("rangePrice") String rangePrice) throws Exception{
        
        return customerPriceService.modifyCustomerPrice(productMasterId, customerMasterId, Integer.parseInt(rangeMin), Integer.parseInt(rangeMax), Float.parseFloat(rangePrice));
    }
    
	/*******************************************************************************
     * 建立者：Guardp  建立日期：-  最後修訂日期：-
     * 功能簡述：跳轉到產品頁面，查詢第一頁的資訊
     * 
     * @param productIdMin
     * @param productIdMax
     * @param productName
     * @param productPriceMin
     * @param productPriceMax
     * @param productSpec
     * @param pageNo
     * @param modelMap
     * @return 
     ********************************************************************************/
    @RequestMapping("deleteProduct")
    @ResponseBody
    public HashMap<String, String> deleteProduct(@RequestParam String productId,String versionNumber) throws Exception{
        
        return productService.deleteProductByProductId(productId,Integer.parseInt(versionNumber));
    }
    
	/*******************************************************************************
     * 建立者：Guardp  建立日期：-  最後修訂日期：-
     * 功能簡述：跳轉到產品頁面，查詢第一頁的資訊
     * 
     * @param productIdMin
     * @param productIdMax
     * @param productName
     * @param productPriceMin
     * @param productPriceMax
     * @param productSpec
     * @param pageNo
     * @param modelMap
     * @return 
     ********************************************************************************/
    @RequestMapping("getProductList")
    @ResponseBody
    public Response getProductList(@RequestParam(value = "inputId",defaultValue = "") String inputId,
            @RequestParam(value = "productIdMin",defaultValue = "") String productIdMin,
            @RequestParam(value = "productIdMax",defaultValue = "") String productIdMax ,
            @RequestParam(value = "productName",defaultValue = "")String productName, 
            @RequestParam(value = "productPriceMin",defaultValue = "0")String productPriceMin,
            @RequestParam(value = "productPriceMax",defaultValue = "0")String productPriceMax,
            @RequestParam(value = "productSpec",defaultValue = "") String productSpec) throws NoSuchFieldException{
        return productService.getProductList(productIdMin,productIdMax,productName,productPriceMin,productPriceMax,productSpec,inputId);
    }
    
	/*******************************************************************************
     * 建立者：Guardp  建立日期：-  最後修訂日期：-
     * 功能簡述：跳轉到產品頁面，查詢第一頁的資訊
     * 
     * @param productIdMin
     * @param productIdMax
     * @param productName
     * @param productPriceMin
     * @param productPriceMax
     * @param productSpec
     * @param pageNo
     * @param modelMap
     * @return 
     ********************************************************************************/
    @RequestMapping("queryCustomerPriceByProductMasterId")
    @ResponseBody
    public HashMap queryCustomerPriceByProductMasterId(@RequestParam("productMasterId") Product productMasterId,@RequestParam(value = "pageNo",defaultValue = "1") String pageNo){
        return customerPriceService.queryCustomerPriceByProductMasterId(productMasterId,pageNo);
    }
}