/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iot.validate;

import iot.dao.entity.Customer;
import iot.dao.entity.CustomerPrice;
import iot.dao.entity.Product;
import java.util.regex.Pattern;
import javax.validation.Valid;
import javax.validation.ValidationException;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;
import org.springframework.ui.ModelMap;

/**
 *
 * @author guardpyang
 */
@Aspect
@Component
public class CustomerPriceValidate {
    
    public static final String CUSTOMERNAME ="[\\u4E00-\\u9FA5]{1,4}|[A-Za-z.\\s]{3,20}";
    public static final String PRODUCTNAME ="[\\u4E00-\\u9FA5]{1,4}|[A-Za-z0-9.\\s]{3,20}";
    public static final String PRICE ="^\\d*(.\\d{0,2})?$";
    public static final String RANGE ="[0-9]{1,9}";
    public static final String PRODUCTID ="PRO[0-9]{0,11}";
    //驗證客戶編號
    public static final String CUSTOMERID = "CUS[0-9]{0,11}";
    
    @Pointcut(value = "execution(* iot.controller.CustomerPriceController.queryCustomerPrice(..))&&args(customerName,productName,priceMin,priceMax,rangeMin,rangeMax,model)")
    public void pointCutQueryCustomerPrice(String customerName,String productName,String priceMin,String priceMax,String rangeMin,String rangeMax,ModelMap model){}
    @Around(value = "pointCutQueryCustomerPrice(customerName,productName,priceMin,priceMax,rangeMin,rangeMax,model)")
    public Object customerPriceQueryValidate(ProceedingJoinPoint pjp,String customerName,String productName,String priceMin,String priceMax,String rangeMin,String rangeMax,ModelMap model) throws Throwable{
        if (!Pattern.matches(CUSTOMERNAME, customerName)&&!customerName.equals("")) {
            throw new ValidationException("您輸入的客戶名稱有誤！中文名至少1個漢字最多只有4個漢字，例如：東方青玉；英文名至少3個字母最多20個字母（包含.與空格），例如：Thomas Alva Edison");
        }
        if (!Pattern.matches(PRODUCTNAME, productName)&&!productName.equals("")) {
            throw new ValidationException("您輸入的產品名稱有誤，請核對信息是否正確！！");
        }
        if (!Pattern.matches(PRICE, priceMin)&&!priceMin.equals("")) {
            throw new ValidationException("您輸入的產品價格有誤，請核對信息是否正確！！");
        }
        if (!Pattern.matches(PRICE, priceMax)&&!priceMax.equals("")) {
            throw new ValidationException("您輸入的產品價格有誤，請核對信息是否正確！！");
        }
        if (!Pattern.matches(RANGE, rangeMin)&&!rangeMin.equals("")) {
            throw new ValidationException("您輸入的產品數量級有誤，請核對信息是否正確！！");
        }
        if (!Pattern.matches(RANGE, rangeMax)&&!rangeMax.equals("")) {
            throw new ValidationException("您輸入的產品數量級有誤，請核對信息是否正確！！");
        }
        return pjp.proceed();
    }
    
    @Pointcut(value = "execution(* iot.controller.CustomerPriceController.addCustomerPrice(..))&&args(customerId,productId,customerPrice)")
    public void pointCutAddCustomerPrice(String customerId,String productId,@Valid CustomerPrice customerPrice){}
    @Around(value = "pointCutAddCustomerPrice(customerId,productId,customerPrice)")
    public Object customerPriceQueryValidate(ProceedingJoinPoint pjp,String customerId,String productId,@Valid CustomerPrice customerPrice) throws Throwable{
//        if((!Pattern.matches(CUSTOMERID, customerId))&&!customerId.equals("")){
//            throw new ValidationException("您輸入的客戶編號錯誤！示例如：CUS20170205001");
//        }
        if(!Pattern.matches(PRODUCTID, productId)){
            throw new ValidationException("您輸入的產品編號錯誤！示例如：PRO20170205001");
        }
        return pjp.proceed();
    }
    
    @Pointcut(value = "execution (* iot.controller.ProductController.modifyCustomerPrice(..))&&args(productMasterId, customerMasterId, rangeMin, rangeMax, rangePrice)")
    public void pointCutModifyCustomerPrice(Product productMasterId,Customer customerMasterId,String rangeMin,String rangeMax,String rangePrice){}
    @Around(value = "pointCutModifyCustomerPrice(productMasterId, customerMasterId, rangeMin, rangeMax, rangePrice)")
    public Object modifyCustomerPriceValidate(ProceedingJoinPoint pjp,Product productMasterId,Customer customerMasterId,String rangeMin,String rangeMax,String rangePrice) throws Throwable{
        if (!Pattern.matches(RANGE, rangeMin)) {
            throw new ValidationException("您輸入的產品數量級有誤，請核對信息是否正確！！");
        }
        if (!Pattern.matches(RANGE, rangeMax)) {
            throw new ValidationException("您輸入的產品數量級有誤，請核對信息是否正確！！");
        }
        if (!Pattern.matches(PRICE, rangePrice)) {
            throw new ValidationException("您輸入的產品價格有誤，請核對信息是否正確！！");
        }
        return  pjp.proceed();
    }
    
    @Pointcut(value = "execution (* iot.controller.ProductController.setCustomerPrice(..))&&args(productMasterId, customerMasterId, rangeMin, rangeMax, rangePrice)")
    public void pointCutSetCustomerPrice(Product productMasterId,Customer customerMasterId,String rangeMin,String rangeMax,String rangePrice){}
    @Around(value = "pointCutSetCustomerPrice(productMasterId, customerMasterId, rangeMin, rangeMax, rangePrice)")
    public Object setCustomerPriceValidate(ProceedingJoinPoint pjp,Product productMasterId,Customer customerMasterId,String rangeMin,String rangeMax,String rangePrice) throws Throwable{
        if (!Pattern.matches(RANGE, rangeMin)) {
            throw new ValidationException("您輸入的產品數量級有誤，請核對信息是否正確！！");
        }
        if (!Pattern.matches(RANGE, rangeMax)) {
            throw new ValidationException("您輸入的產品數量級有誤，請核對信息是否正確！！");
        }
        if (!Pattern.matches(PRICE, rangePrice)) {
            throw new ValidationException("您輸入的產品價格有誤，請核對信息是否正確！！");
        }
        return  pjp.proceed();
    }
}
