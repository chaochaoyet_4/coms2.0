/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iot.validate;

import java.util.regex.Pattern;
import javax.validation.ValidationException;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;
import org.springframework.ui.ModelMap;

/**
 *
 * @author guardpyang
 */
@Aspect
@Component
public class ProductValidate {
    
    public static final String PRODUCTID ="PRO[0-9]{0,11}";
    public static final String PRODUCTNAME ="[\\u4E00-\\u9FA5]{1,4}|[A-Za-z0-9.\\s]{3,20}";
    public static final String PRODUCTPRICE ="^\\d*(.\\d{0,2})?$";
    public static final String PRODUCTSPEC ="[A-Za-z0-9\\u4E00-\\u9FA5]{1,20}";
    public static final String PAGENO ="[1-9]{1}[0-9]{0,4}";
    
//    public static void main(String[] args) {
//        String test1 = "1999";
//        System.out.println(Pattern.matches(PAGENO, test1));
//    }
    @Pointcut(value = "execution(* iot.controller.ProductController.productMasterQuery(..))&&args(productIdMin,productIdMax,productName,productPriceMin,productPriceMax,productSpec,pageNo,model)")
    public void pointCutProductMasterQuery(String productIdMin,String productIdMax,String productName,String productPriceMin,String productPriceMax,String productSpec,String pageNo,ModelMap model){}
    @Around(value = "pointCutProductMasterQuery(productIdMin,productIdMax,productName,productPriceMin,productPriceMax,productSpec,pageNo,model)")
    public Object productMasterQueryValidate(ProceedingJoinPoint pjp,String productIdMin,String productIdMax,String productName,String productPriceMin,String productPriceMax,String productSpec,String pageNo,ModelMap model) throws Throwable{
        if (!Pattern.matches(PAGENO, pageNo)) {
            throw new ValidationException("您輸入的頁碼錯誤！");
        }
        if (!Pattern.matches(PRODUCTID, productIdMax)&&!productIdMax.equals("")) {
            throw new ValidationException("您輸入的產品編號錯誤！示例如：PRO20170205001。");
        }
        if (!Pattern.matches(PRODUCTID, productIdMin)&&!productIdMin.equals("")) {
            throw new ValidationException("您輸入的產品編號錯誤！示例如：PRO20170205001。");
        }
        if (!Pattern.matches(PRODUCTNAME, productName)&&!productName.equals("")) {
            throw new ValidationException("您輸入的產品名稱有誤，請核對信息是否正確！！");
        }
        if (!Pattern.matches(PRODUCTSPEC, productSpec)&&!productSpec.equals("")) {
            throw new ValidationException("您輸入的產品規格有誤，請核對信息是否正確！！");
        }
        if (!Pattern.matches(PRODUCTPRICE, productPriceMax)&&!productPriceMax.equals("")) {
            throw new ValidationException("您輸入的產品價格有誤，請核對信息是否正確！！");
        }
        if (!Pattern.matches(PRODUCTPRICE, productPriceMin)&&!productPriceMin.equals("")) {
            throw new ValidationException("您輸入的產品價格有誤，請核對信息是否正確！！");
        }
        return pjp.proceed();
    }
    
    @Pointcut(value = "execution(* iot.controller.ProductController.addProduct(..))&&args(productStandardPrice,productName,discountStatus,productSpec)")
    public void pointCutAddProduct(String productStandardPrice,String productName,boolean discountStatus,String productSpec){}
    @Around(value = "pointCutAddProduct(productStandardPrice,productName,discountStatus,productSpec)")
    public Object productMasterAddValidate(ProceedingJoinPoint pjp,String productStandardPrice,String productName,boolean discountStatus,String productSpec) throws Throwable{
        String productPrice = String.valueOf(productStandardPrice);
        if (!Pattern.matches(PRODUCTNAME, productName)) {
            throw new ValidationException("您輸入的產品名稱有誤，請核對信息是否正確！！");
        }
        if (!Pattern.matches(PRODUCTSPEC, productSpec)) {
            throw new ValidationException("您輸入的產品規格有誤，請核對信息是否正確！！");
        }
        if (!Pattern.matches(PRODUCTPRICE, productPrice)) {
            throw new ValidationException("您輸入的產品價格有誤，請核對信息是否正確！！");
        }
        return pjp.proceed();
    }
    
    @Pointcut(value = "execution(* iot.controller.ProductController.modifyProduct(..))&&args(productId,productStandardPrice,discountStatus)")
    public void pointCutModifyProduct(String productId,String productStandardPrice,boolean discountStatus){}
    @Around(value = "pointCutModifyProduct(productId,productStandardPrice,discountStatus)")
    public Object productMasterModifyValidate(ProceedingJoinPoint pjp,String productId,String productStandardPrice,boolean discountStatus) throws Throwable{
        String productPrice = String.valueOf(productStandardPrice);
        if (!Pattern.matches(PRODUCTPRICE, productPrice)) {
            throw new ValidationException("您輸入的產品價格有誤，請核對信息是否正確！！");
        }
        return pjp.proceed();
    }
}
