/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iot.validate;

import java.util.regex.Pattern;
import javax.validation.ValidationException;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;
import org.springframework.ui.ModelMap;


/**
 *
 * @author guardpyang
 */
@Aspect
@Component
public class CustomerValidate {
    
    //驗證客戶編號
    public static final String CUSTOMERID = "CUS[0-9]{0,11}";
    //驗證客戶姓名
    public static final String CUSTOMERNAME ="[\\u4E00-\\u9FA5]{1,4}|[A-Za-z.\\s]{3,20}";
    //驗證郵件格式
    public static final String CUSTOMERMAIL ="^([a-zA-Z_0-9-])+@([a-zA-Z_0-9-])+(.[a-zA-Z_0-9-])+$";
//    public static final String CUSTOMERPHONE = "^1[3|4|5|7|8][0-9]{9}$";

    /**
     * *****************************************************************************
 建立者：Guardp 建立日期：- 最後修訂日期：- 功能簡述：查詢數量級、價格輸入框下拉列表數據
     * @param customerIdMin
     * @param customerIdMax
     * @param customerName
     * @return
     */
    @Pointcut(value = "execution(* iot.controller.CustomerController.customerQuery(..))&&args(customerIdMin,customerIdMax,customerName,model)")
    public void pointCutCustomerQuery(String customerIdMin,String customerIdMax,String customerName,ModelMap model){
        
    }
    @Around(value = "pointCutCustomerQuery(customerIdMin,customerIdMax,customerName,model)")
    public Object customerQueryValidation(ProceedingJoinPoint pjp,String customerIdMin,String customerIdMax,String customerName,ModelMap model) throws Throwable{
        if((!Pattern.matches(CUSTOMERID, customerIdMax))&&!customerIdMax.equals("")){
            throw new ValidationException("您輸入的客戶編號錯誤！示例如：CUS20170205001");
        }
        if(!Pattern.matches(CUSTOMERID, customerIdMin)&&!customerIdMin.equals("")){
            throw new ValidationException("您輸入的客戶編號錯誤！示例如：CUS20170205001");
        }
        if (!Pattern.matches(CUSTOMERNAME, customerName)&&!customerName.equals("")) {
            throw new ValidationException("您輸入的客戶名稱有誤！中文名至少1個漢字最多只有4個漢字，例如：東方青玉；英文名至少3個字母最多20個字母（包含.與空格），例如：Thomas Alva Edison");
        }
        
        return pjp.proceed();
    }
    
}
